const { request, response } = require('express');
const express = require('express')
const db  = require('../db')
const utils  = require('../utils')

const router  = express.Router();

// -- Now create server side application using Node and express Containerized it and Perform following application 

// -- 1. GET  --> Display Movie using name from Containerized MySQL

// -- 2. POST --> ADD Movie data into Containerized MySQL table

// -- 3. UPDATE --> Update Release_Date and Movie_Time into Containerized MySQL table

// -- 4. DELETE --> Delete Movie from Containerized MySQL

router.get('/',(request,response)=>{

    const query = `select * from movie`;

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result));
    });
});

router.post('/',(request,response)=>{

    const {movie_title,movie_release_date,movie_time,director_name} = request.body;

    const query = `insert into movie(movie_title,movie_release_date,movie_time,director_name) values('${movie_title}','${movie_release_date}',
    '${movie_time}','${director_name}')`;

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result));
    });
});

router.patch('/update/:id',(request,response)=>{

    const {id} = request.params;
  
    const {movie_release_date,movie_time} = request.body;

    const query = `update movie set movie_release_date='${movie_release_date}',
         movie_time='${movie_time}' where movie_id=${id}`;

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result));
    });
});


router.delete('/delete/:id',(request,response)=>{

    const {id} = request.params;
  
    const query = `delete from movie where movie_id = ${id};`

    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result));
    });
});

module.exports = router;